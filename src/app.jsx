import React, {Component} from 'react';
import '../styles/index.scss';

// custom imports
import Header 		from './components/Header';
import Board 			from './components/Board';

export default class App extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		let list = { "listOfList": [
			{
				"listId": "l0",
				"title": "Default List 0",
			},
			{
				"listId": "l1",
				"title": "Default List 1",
			}
		]};

		localStorage.setItem('listOfList', JSON.stringify(list));
	}

  render() {
    return (
      <div>
        <Header />
        <Board />
      </div>
    )
  }
}
