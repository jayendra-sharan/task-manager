// this file creates the main layout, also called board here.
// this board will hold the place for creating list

import React, { Component, PropTypes } from 'react';

// custom imports
import ListOfList 			from '../containers/list/ListOfList';

class Board extends Component {

	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="layout main-board">
				<div className="mod-board-top">
					<span className="col-2 mod-board-title text-left">
						Sample Board
					</span>
					<span className="col-2 mod-board-actions text-right">
						Actions
					</span>
					<div className="mod-board-main-content">
						<ListOfList />
					</div>
				</div>
			</div>
		)
	}
}

export default Board;