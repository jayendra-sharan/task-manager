// this file creates the presentational component for header.

import React, { Component, PropTypes } from 'react';

class Header extends Component {

	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="layout main-header">
				<div className="col-2 mod-page-title text-left">Ixigo Assignment</div>
				<div className="col-2 mod-logged-in-user text-right">Username</div>
			</div>
		)
	}

}

export default Header;
