export const ADD_LIST = 'ADD_LIST';
export const UPDATE_LIST_TITLE = 'UPDATE_LIST_TITLE';
export const ADD_CARD_TO_LIST = 'ADD_CARD_TO_LIST';
export const REMOVE_CARD_FROM_LIST = 'REMOVE_CARD_FROM_LIST';
export const REMOVE_LIST = 'REMOVE_LIST';
export const COMMIT_CHANGES = 'COMMIT_CHANGES';
export const LOAD_DATA = 'LOAD_DATA';
export const UPDATE_CARD_IN_LIST = 'UPDATE_CARD_IN_LIST';

let randomId = () => {
	const num = Math.floor((Math.random() * 888) * (Math.random() * 123));
	if (num > 10) {
		return num;
	} else {
		randomId();
	}
}

export const addList = (title) => {
	return {
		type: ADD_LIST,
		id: "l" + randomId(),
		title,
		cards: []
	}
}

export const updateListTitle = (id, newTitle) => {
	return {
		type: UPDATE_LIST_TITLE,
		id,
		title: newTitle
	}
}

export const addCardToList = (listId, card) => {
	card.id = card.id || "c" + listId + randomId();
	return {
		type: ADD_CARD_TO_LIST,
		listId,
		card
	}
}

export const removeCardFromList = (listId, card) => {
	return {
		type: REMOVE_CARD_FROM_LIST,
		listId,
		card
	}
}

export const removeList = (listId) => {
	return {
		type: REMOVE_LIST,
		listId
	}
}

export const commitChanges = () => {
	return {
		type: COMMIT_CHANGES
	}
}

export const loadData = () => {
	return {
		type: LOAD_DATA
	}
}

export const updateCardInList = (listId, card) => {
	return {
		type: UPDATE_CARD_IN_LIST,
		listId,
		card
	}
}
