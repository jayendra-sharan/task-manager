import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from './app.jsx';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import taskManagerApp from './reducers';

let store = createStore(taskManagerApp);

render(<AppContainer>
					<Provider store={store}>
						<App/>
					</Provider>
				</AppContainer>,
			document.querySelector("#app")
		);

if (module && module.hot) {
  module.hot.accept('./app.jsx', () => {
    const App = require('./app.jsx').default;
    render(
      <AppContainer>
        <Provider store={store}>
						<App/>
					</Provider>
      </AppContainer>,
      document.querySelector("#app")
    );
  });
}
