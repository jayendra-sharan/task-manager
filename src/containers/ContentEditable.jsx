// creates a component for editing an html element.

import React, { PropTypes, Component } from 'react';

class ContentEditable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      html: this.props.html
    }
  }

  handleChange(event) {
    var html = event.target.textContent;
    this.props.onBlur(event, html, this.props.name);
  }

  render (){
    return (
      <div
        name={this.props.name}
        className={this.props.className} onBlur={this.handleChange.bind(this)}
        contentEditable="true" dangerouslySetInnerHTML={{__html: this.props.html}}>
      </div>
    );
  }
}

export default ContentEditable;
