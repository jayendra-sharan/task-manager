// add form.

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

// custom imports
import { addList, commitChanges } from '../../actions/';

class AddList extends Component {
	constructor(props){
		super(props);

		this.state = {
			listTitle: ""
		}
	}

	/**
	 * onClick function to handle the changed value of input box.
	 */
	_onChange(event) {
		event.preventDefault();
		this.setState({
			listTitle: event.target.value
		});
	}

	/**
	 * on click handler for save button.
	 */

	_onSave(event) {
		event.preventDefault();
		const {dispatch} = this.props;
		dispatch(addList(this.state.listTitle))
		dispatch(commitChanges());
		// clear the form.
		this.setState({ listTitle: '' });
		this.props.onClick(event, "hide");
	}

	/**
	 * on click handler for cancel button.
	 */

	_onCancel(event) {
		event.preventDefault();
		this.props.onClick(event, "hide");
	}

	render() {
		return (
			<form
				onSubmit={this._onSave.bind(this)}
				className="mod-list-add-input">
				<input
					type="text"
					onChange={this._onChange.bind(this)}
					value={this.state.listTitle}
					placeholder="Add a list..."
					/>
					<div className="mod-list-add-control">
						<button
							className="btn btn-save"
							type="submit">
							Save
						</button>

						<button
							className="btn btn-cancel"
							onClick={this._onCancel.bind(this)}
							>
							Cancel
						</button>
					</div>
			</form>
		)
	}
}

AddList = connect()(AddList);

export default AddList;
