// creates individual lists
import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
// import {Editor, EditorState} from 'draft-js';

// custom imports
import AddCard						from	'../card/AddCard';
import Card								from '../card/Card';
import ContentEditable		from '../ContentEditable';
import {
	updateListTitle,
	addCardToList,
	removeCardFromList,
	removeList,
	commitChanges
	} 	from '../../actions';

class List extends Component {
	constructor(props) {
		super();
		this.state = {
			showAddCard: false,
			hasCards: false,
			editCard: false
		}
	}

	// handles the content editable event.

	_onContentEdit(event, content) {
		event.preventDefault();
		const {dispatch, listItem} = this.props;
		dispatch(updateListTitle(listItem.id, content));
		dispatch(commitChanges());
	}

	// handles the key press event to check the if enter key is pressed.
	_onKeyPress(event) {
		if (event.key ==="Enter") {
			event.target.blur();
			this._onContentEdit(event);
		}
	}

	// handles click on card label, hides label, shows form.
	_onAddCardLabelClick(event) {
		event.preventDefault();
		this.setState({
			showAddCard: true
		});
	}

	// handles click event on add card form

	_onAddCardClick(event, action, card) {
		if (action === "hide") {
			this.setState({
				showAddCard: false
			});
		} else if(action === "save") {
			// save card
			const {dispatch, listItem} = this.props;
			dispatch(addCardToList(listItem.id, card));
			dispatch(commitChanges());
		}
	}

	// function to delete the list

	_onDeleteClick(event) {
		const {dispatch} = this.props;
		dispatch(removeList(this.props.listId));
		dispatch(commitChanges());
	}

	_onDrop(event) {
		const {dispatch} = this.props;
		if (this.props.listItem.id !== event.dataTransfer.getData('sourceListId')) {
			dispatch(addCardToList(this.props.listItem.id, JSON.parse(event.dataTransfer.getData('card'))));
			dispatch(removeCardFromList(event.dataTransfer.getData('sourceListId'), JSON.parse(event.dataTransfer.getData('card'))));
			dispatch(commitChanges());
		} else {
			event.preventDefault();
		}

	}

	_onDragStart(event, source, card) {
		// event.preventDefault();
		event.dataTransfer.setData('card', JSON.stringify(card));
		event.dataTransfer.setData('sourceListId', source);
	}

	// function to handle clicks on card for action.
	_onCardClick(event, card, action) {
		const {dispatch} = this.props;
		if (action === "hide") {
			this.setState({
				editCard: false
			})
		} else if (action === "delete") {
			dispatch(removeCardFromList(this.props.listItem.id, card));
			dispatch(commitChanges());
		} else if (action="edit") {
			this.setState({
				editCard: true
			})
		} else if (action === "update") {
			dispatch(updateCardInList(listItem.id, card));
			dispatch(commitChanges());
			this.setState({
				editCard: false
			})
		}
	}


	render() {
		const {listItem, hasCards, listId} = this.props;
		return (
			<div
				className="mod-list-item"
				key={listItem.id}
				onDrop={this._onDrop.bind(this)}
				onDragOver={e => e.preventDefault()}
				>
				<span
					onClick= {this._onDeleteClick.bind(this)}
					className="mod-list-delete">
					&otimes;
				</span>
				<ContentEditable
					className="mod-list-title"
					onBlur={this._onContentEdit.bind(this)} html={listItem.title} />
				{
					hasCards &&
					listItem.cards.map(card => {
							return (
								<Card
									listId={listId}
									draggable="true"
									onDragStart={this._onDragStart.bind(this)}
									key={card.id}
									card={card}
									onClick={this._onCardClick.bind(this)}	/>
								)
					})
				}
				{
					this.state.showAddCard &&
					<AddCard
						onClick={this._onAddCardClick.bind(this)}
						className="mod-list-add-card-form" />
				}
				{
					!this.state.showAddCard &&
					<div
						onClick={this._onAddCardLabelClick.bind(this)}
						className="mod-list-add-card">
						add a card...
					</div>
				}
			</div>
		)
	}
}

export default connect()(List);
