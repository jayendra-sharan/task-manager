// this is a stateful component and will display all available lists.

import React, { PropTypes, Component} from 'react';
import { connect } from 'react-redux';

//custom imports
import List 				from 	'./List';
import AddList 			from 	'./AddList';
import { loadData } from '../../actions';

class ListOfList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			listOfList: [],
			showAddForm: false,
			listTitle: ""
		}
	}

	componentDidMount() {
		this.props.dispatch(loadData());
	}

	/**
	 * onClick function to handle the changed value of input box.
	 */

	_onClick(event, action) {
 		event.preventDefault();
 		this.setState({
 			showAddForm: true
 		});
	}

	/**
	 * onClick function to handle the changed value of input box.
	 */

	_onAddListFormClick(event, action) {
 		event.preventDefault();
 		if (action === "hide") {
 			this.setState({
 				showAddForm: false
 			})
 		}
	}

	render() {
		const { lists } = this.props;
		let id = 0;
		return (
			<ul className="mod-top-level-list">{
					lists.map((item) => {
						let hasCards = item.cards.length > 0 ? true : false;
						return (
							<li
								className="mod-list"
								key={id++}>
								<List
									hasCards={hasCards}
									listId={item.id}
									key={item.id}
									listItem={item} />

							</li>
						)
					})
				}
				<li key="add" className="mod-list mod-list-add-form">
					{
						!this.state.showAddForm	 &&
						<div
							className="mod-list-add-label"
							onClick={this._onClick.bind(this)} >
							Add a list...
						</div>
					}
					{
						this.state.showAddForm &&
						<AddList
							show={this.state.showAddForm}
							onClick={this._onAddListFormClick.bind(this)}
						/>

					}
				</li>
			</ul>
		)
	}
}

const mapStateToProps = (state) => {
	const lists = state.lists || [];
	return {
		lists
	};
}

export default connect(mapStateToProps)(ListOfList);
