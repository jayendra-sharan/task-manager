// add card form

import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';


import {
  updateCardInList,
  commitChanges
} from '../../actions';

class EditCard extends Component {
  constructor(props) {
    super();
  }

  // handler for form submit event
  _onFormSubmit(event) {
    event.preventDefault();
    const card = {
      cardTitle: this.refs.cardTitle.value,
      cardDesc: this.refs.cardDesc.value,
      status: this.refs.status.value,
      id: this.props.card.id,
      user: this.refs.users.value.split(",")
    }

    const {dispatch} = this.props;
    dispatch(updateCardInList(this.props.listId, card));
    dispatch(commitChanges());

    // hide the card editor.
    this.props.onClick(event, undefined, "hide");
  }

  // hanler for form cancel event.
  _onFormCancel(event) {
    event.preventDefault();
    this.props.onClick(event, undefined, "hide");
  }

  // handler for change event to keep track of value of input fields.
  _onChange(event) {
    event.preventDefault();
    const target = event.target.name;
    const value = event.target.value;
    const {card} = this.state;
    card[target] = value;

    this.setState({card});
  }

  componentDidMount() {
    const {card} = this.props;
    this.setState({
      card
    })
  }

  _onBlur(event) {
    debugger;
  }

  render() {
    const {card} = this.props;
    debugger;
    return(
      <form id="form-add-card" onSubmit={this._onFormSubmit.bind(this)}>
        <input
          type="text"
          ref="cardTitle"
          className="mod-card-title"
          defaultValue={card.cardTitle}
        />

        <textarea
          ref="cardDesc"
          className="mod-card-desc"
          defaultValue={card.cardDesc}
        />

        <select ref="status" defaultValue={card.status} onChange={this._onChange.bind(this)}>
          <option value="New">New</option>
          <option value="In Progress">In Progress</option>
          <option value="Pending">Pending</option>
        </select>

        <input
          type="text"
          ref="users"
          placeholder="tag users, comma separted"
          className="mod-card-users"
          defaultValue={card.user}
        />

        <div className="form-control">
          <button
            type="submit"
            form="form-add-card"
            className="btn btn-save">
            Save
          </button>

          <button
            type="cancel"
            onClick={this._onFormCancel.bind(this)}
            className="btn btn-cancel">
            Cancel
          </button>
        </div>
      </form>
    )
  }
}

export default connect()(EditCard);
