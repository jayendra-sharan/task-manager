// component to display individual cards.

import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';

// custom imports
import {
  updateCardInList
} from '../../actions';
import ContentEditable from '../ContentEditable';
import EditCard from './EditCard';

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editCard: false
    }
  }

  _onDragStart(event) {
    const { card } = this.props;
    // event.dataTransfer.setData('item', card.id);
    this.props.onDragStart(event, this.props.listId, card);
  }

  // function to handle on click on delete
  _deleteCard(event) {
    this.props.onClick(event, this.props.card, "delete");
  }

  // function to handle edit.

  _editCard(event) {
    // add edit class to card
    this.setState({
      editCard: true
    })
    this.props.onClick(event, this.props.card, "edit");
  }

  // update changes in the card.
  _onContentEdit(event, newText, field) {
    const {dispatch, card} = this.props;
    dispatch(updateCardInList(this.props.listId, card.id, field, newText));
    dispatch(commitChanges());
  }

  // updates the card
  _updateCard(event, card, action) {
    if (action === "hide") {
      this.setState({
        editCard: false
      })
    } else {

    }
  }

  render() {
    const {card} = this.props;
    return (
      <span>
        {
          !this.state.editCard &&
          <div className="mod-card-top-level"
            draggable="true"
            onDragStart={this._onDragStart.bind(this)}>

            <div className="mod-card-show-title">
                {card.cardTitle}
            </div>

            <div className="mod-card-show-description">
              {card.cardDesc}
            </div>

            <div className="mod-card-show-meta">
              <span className="text-left mod-card-show-col">
                {card.status}
              </span>
              <span className="text-right mod-card-show-col">
                {card.user}
              </span>
            </div>

            <div className="mod-card-show-meta mod-card-actions">
              <span
                onClick={this._editCard.bind(this)}
                className="text-left mod-card-show-col mod-card-edit">
                edit
              </span>
              <span
                onClick={this._deleteCard.bind(this)}
                className="text-right mod-card-show-col mod-card-edit">
                delete
              </span>
            </div>
          </div>
        }
        {
          this.state.editCard &&
          <EditCard listId={this.props.listId} onClick={this._updateCard.bind(this)} card={card} />
        }
      </span>

    )
  }
}

export default connect()(Card);
