// add card form

import React, {PropTypes, Component} from 'react';

class AddCard extends Component {
  constructor(props) {
    super();
    this.state = {
      card : {
        cardTitle: "",
        cardDesc: "",
        status: "New",
        user: ""
      }
    }
  }

  // handler for form submit event
  _onFormSubmit(event) {
    event.preventDefault();
    this.props.onClick(event, "save", this.state.card);

    // hide the card editor.
    // set content to empty

    this.setState({
      card: {
        cardTitle: "",
        cardDesc: ""
      }
    });

    this.props.onClick(event, "hide");
  }

  // hanler for form cancel event.
  _onFormCancel(event) {
    event.preventDefault();

    this.props.onClick(event, "hide");
  }

  // handler for change event to keep track of value of input fields.
  _onChange(event) {
    event.preventDefault();
    const target = event.target.name;
    const value = event.target.value;
    const {card} = this.state;
    card[target] = value;

    this.setState({card});
  }

  render() {
    return(
      <form id="form-add-card" onSubmit={this._onFormSubmit.bind(this)}>
        <input
          type="text"
          name="cardTitle"
          className="mod-card-title"
          value={this.state.card.cardTitle}
          placeholder="Card Title..."
          onChange={this._onChange.bind(this)}
          />
        <textarea
          name="cardDesc"
          className="mod-card-desc"
          placeholder="Add Description..."
          value={this.state.card.cardDesc}
          onChange={this._onChange.bind(this)}
          />

        <select name="status" onChange={this._onChange.bind(this)}>
          <option value="New">New</option>
          <option value="In Progress">In Progress</option>
          <option value="Pending">Pending</option>
        </select>

        <input
          type="text"
          name="user"
          placeholder="add users, comma separted"
          className="mod-card-users"
          value={this.state.card.user}
          onChange={this._onChange.bind(this)}
        />

        <div className="form-control">
          <button
            type="submit"
            form="form-add-card"
            className="btn btn-save">
            Save
          </button>

          <button
            type="cancel"
            onClick={this._onFormCancel.bind(this)}
            className="btn btn-cancel">
            Cancel
          </button>
        </div>
      </form>
    )
  }
}

export default AddCard;
