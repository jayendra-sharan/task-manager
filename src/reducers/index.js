import { combineReducers } from 'redux';
import lists from './lists';

const taskManagerApp = combineReducers({
	lists
});
export default taskManagerApp;