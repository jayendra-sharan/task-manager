import {
	ADD_LIST,
	UPDATE_LIST_TITLE,
	ADD_CARD_TO_LIST,
	REMOVE_CARD_FROM_LIST,
	REMOVE_LIST,
	COMMIT_CHANGES,
	LOAD_DATA,
	UPDATE_CARD_IN_LIST
} from '../actions'
import {
	saveToLocalStorage,
	getFromLocalStorage
} from '../Util';

const APP_STATE = 'state';

const list = (state = {}, action) => {
	switch (action.type) {
		case ADD_LIST:
			return {
				id: action.id,
				title: action.title,
				cards: []
			};

		case UPDATE_LIST_TITLE:
			if (state.id !== action.id) {
				return state;
			} else {
				return Object.assign({}, state, {title: action.title});
			}
		case ADD_CARD_TO_LIST:
		if (state.id !== action.listId) {
			return state;
		} else {
			return Object.assign({}, state, {cards: [...state.cards, action.card]});
		}
		case REMOVE_CARD_FROM_LIST:
			if (state.id !== action.listId) {
				return state;
			} else {
				return Object.assign({}, state, {cards: state.cards.filter(item => item.id !== action.card.id)});
			}
		case UPDATE_CARD_IN_LIST:
			if (state.id !== action.listId) {
				return state;
			} else {
				return Object.assign({}, state, {
					cards: state.cards.reduce((acc, item) => {
						if (item.id === action.card.id) {
							acc.push(action.card);
						} else {
							acc.push(item);
						}
						return acc;
					}, [])
				})
			}
		default:
			return state;
	}
}

// initially the list will be empty.

let initialState = [];

const lists = (state = initialState, action ) => {
	switch (action.type) {
		case ADD_LIST:
			return [
				...state,
				list(undefined, action)
			];
		case UPDATE_LIST_TITLE:
		case ADD_CARD_TO_LIST:
		case REMOVE_CARD_FROM_LIST:
			return state.map(l => list(l, action));

		case REMOVE_LIST:
			return state.filter(item => item.id !== action.listId);

		case LOAD_DATA:
			state = getFromLocalStorage(APP_STATE);
			if (state) {
				return state;
			} else {
				return [];
			}

		case COMMIT_CHANGES:
			saveToLocalStorage(APP_STATE, state);
			return state;

		case UPDATE_CARD_IN_LIST:
			return state.map(l=> list(l, action));
		default:
			return state;
	}
}

export default lists;
