# Task Manager Application #

This application allows you to create multiple lists and within that list multiple cards. All list titles are editable and can be deleted. A card can be transferred from one to another list and the can be deleted and edited.

### How to set up? ###

Clone this repository by following command.

``
git clone https://jayendra-sharan@bitbucket.org/jayendra-sharan/task-manager.git <directory-optional>
``

``
cd <directory name>
``

``
npm install
``

Once the installation of all the dependencies completed, run following command to run the application.

``
npm start
``

Open the web browser to `http://localhost:8888/`

To build the code

``
npm run build
``